import { Component, OnInit, Inject, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Observable, Observer, Subscription } from 'rxjs';
import { SelectionModel } from '@angular/cdk/collections';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/@core/utils/common.service';
import { AuthService } from 'src/app/@core/services/base/00auth.service';
import { FabcarService } from 'src/app/@core/services/base/70district.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-c7-fabcar',
  templateUrl: './c7-fabcar.component.html',
  styleUrls: ['./c7-fabcar.component.scss'],
})
export class C7FabcarComponent implements OnInit, AfterViewInit, OnDestroy {

  //subscription
  subscription: Subscription[] = [];

  /** for table */
  displayedColumns: string[] = [
    'select',
    'Key',
    'colour',
    'make',
    'model',
    'owner',

    'edit',
  ];

  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  selection = new SelectionModel<any>(true, []);

  // update pagination
  pageIndex = 0;
  pageLength = 0;
  pageSize = 50;
  pageSizeOptions: number[] = [10, 20, 50, 100];

  /**
   * onPageChange
   */
  onPageChange(event) {
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;

    this.onLoadDataGrid();
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    if (this.dataSource) {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
    }
    return null;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach((row) => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1
      }`;
  }
  // end for table;

  // data reference binding
  provinceDatas: any[] = [];


  // role
  staff: any;

  // data permission
  isPermissionMenu1: boolean = false;

  // condition fillter
  conditonFilter: string = '';
  conditions: any[] = [];

  // model
  idProvinceModel: any = '0';


  /**
   * 
   * @param router 
   * @param dialog 
   * @param sinhVienService 
   * @param commonService 
   * @param authService 
   */
  constructor(
    private router: Router,
    public dialog: MatDialog, 

    private sinhVienService: FabcarService,
    private commonService: CommonService,
    private authService: AuthService,
    public spinner: NgxSpinnerService
  ) {
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    // load data user
    this.onLoadDataGrid();
  }

  /**
  * ng After View Init
  */
  ngAfterViewInit(): void {
    // scroll top screen
    window.scroll({ left: 0, top: 0, behavior: 'smooth' });
  }

  /**
   * ngOnDestroy
   */
  ngOnDestroy() {
    this.subscription.forEach((item) => {
      item.unsubscribe();
    });
  }
   

  /**
   * add New Condition To List
   * @param condition
   */
  addNewConditionToList(condition) {
    // check exists
    let flg = false;
    let i;

    // check condition exists
    for (i = 0; i < this.conditions.length; i++) {
      if ((this.conditions[i].key == condition.key)) {
        flg = true;
        break;
      }
    }

    // remove old key
    if (flg) {
      this.conditions.splice(i, 1);
    }

    // insert new seach condition if !=0
    if (condition.value != '0') {
      this.conditions.splice(0, 0, condition);
    }

    // render new condition filter
    this.createConditionFilter();

    // load grid with new condition
    this.onLoadDataGrid();
  }

  /**
   * create Condition Filter
   */
  createConditionFilter() {
    this.conditonFilter = '';
    this.conditions.forEach((item) => {
      if (this.conditonFilter == '') {
        this.conditonFilter = item.key + "=" + item.value + "";
      } else {
        this.conditonFilter += '&' + item.key + "=" + item.value + "";
      }
    });
    if (this.conditonFilter != '') {
      this.conditonFilter = '&' + this.conditonFilter;
    }
  }

  /**
   * get data grid
   */
  onLoadDataGrid() {
    this.spinner.show();
    this.subscription.push(
      this.sinhVienService.get(
      ).subscribe((data) => {
        data = JSON.parse(data['response']);
        console.log('phuong', data);
        // get Length Of Page
        this.pageLength = data.length;
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.sort = this.sort;
        this.selection = new SelectionModel<any>(true, []);

        this.spinner.hide();
      })
    );
  }


  /**
   * on Delete Click
   */
  onBtnDelClick() {
    // get listId selection example: listId='1,2,6'
    let listId = '';
    this.selection.selected.forEach((item) => {
      if (listId == '') {
        listId += item.id;
      } else {
        listId += ',' + item.id;
      }
    });

    const param = { listid: listId };

    // start delete
    if (listId !== '') {
      this.subscription.push(
        this.sinhVienService.delete(listId).subscribe((data) => {
          if (data) {
            // load data grid
            this.onLoadDataGrid();

            //scroll top
            window.scroll({ left: 0, top: 0, behavior: 'smooth' });

            // show toast success
            this.commonService.showSuccess('Xóa thành công.');
          } else {
            // load data grid
            this.onLoadDataGrid();

            //scroll top
            window.scroll({ left: 0, top: 0, behavior: 'smooth' });

            // show error
            this.commonService.showError('Xóa không thành công');
          }
        })
      );
    } else {
      // show toast warning
      this.commonService.showWarning('Vui lòng chọn 1 mục để xóa.');
    }
    this.selection = new SelectionModel<any>(true, []);
  }

  /**
   * on insert data
   * @param event
   */
  onBtnInsertDataClick() {
    const dialogRef = this.dialog.open(C7FabcarDialog, {
      width: '80%',
      height: '80%',
      maxWidth: '100%',
      maxHeight: '100%',
      data: {
        type: 0,
        id: 0,
        idProvinceModel: this.idProvinceModel,

      },
      panelClass: 'custom-dialog',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.onLoadDataGrid();
      }
    });
  }

  /**
   * on update data
   * @param event
   */
  onBtnUpdateDataClick(row) {
    const dialogRef = this.dialog.open(C7FabcarDialog, {
      width: '80%',
      height: '80%',
      maxWidth: '100%',
      maxHeight: '100%',
      data: { type: 1, input: row },
      panelClass: 'custom-dialog',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.onLoadDataGrid();
      }
    });
  }
}

/**
 * Component show thông tin để insert hoặc update
 */
@Component({
  selector: 'c7-fabcar-dialog',
  templateUrl: 'c7-fabcar-dialog.html',
  styleUrls: ['./c7-fabcar.component.scss'],
})
export class C7FabcarDialog implements OnInit, OnDestroy {

  observable: Observable<any>;
  observer: Observer<any>;
  type: number;

  //subscription
  subscription: Subscription[] = [];

  // init input value
  input: any = {
    Key: '',
    colour: '',
    make: '',
    model: '',
    owner: '',
  };

  //form
  form: FormGroup;

  // sex
  sexs: any[] = [
    { value: '1', viewValue: 'Nam' },
    { value: '0', viewValue: 'Nữ' },
  ];

  // data reference binding
  provinceDatas: any[] = [];


  // binding uploads image or file


  // isUpdate
  isUpdate: boolean = false;

  /**
   * constructor
   * @param dialogRef
   * @param data
   * @param api
   * @param formBuilder
   */
  constructor(
    public dialogRef: MatDialogRef<C7FabcarDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private formBuilder: FormBuilder, 

    private sinhVienService: FabcarService,
    private commonService: CommonService,
    public spinner: NgxSpinnerService
  ) {
    this.type = data.type;

    console.log('phuong', data);

    // nếu là update
    if (this.type == 1) {
      this.input = data.input;

      // mapping data filter
      this.input.Key = data.input.Key;
      this.input.colour = data.input.Record.colour;
      this.input.make = data.input.Record.make;
      this.input.model = data.input.Record.model;
      this.input.owner = data.input.Record.owner;
    } else {

    }

    // add validate for controls
    this.form = this.formBuilder.group({
      Key: [
        null,
        [
          Validators.required,
        ],
      ],
      colour: [
        null,
        [
          Validators.required,
        ],
      ],
      make: [
        null,
        [
          Validators.required,
        ],
      ],
      model: [
        null,
        [
          Validators.required,
        ],
      ],
      owner: [
        null,
        [
          Validators.required,
        ],
      ],
    });

    // xử lý bất đồng bộ
    this.observable = Observable.create((observer: any) => {
      this.observer = observer;
    });
  }

  /**
   * onInit
   */
  ngOnInit() {

  }

  /**
   * ngOnDestroy
   */
  ngOnDestroy() {
    this.subscription.forEach((item) => {
      item.unsubscribe();
    });
  }


  /**
   * on Btn Submit Click
   */
  onBtnSubmitClick(): void {
    this.spinner.show();

    // disable button update
    this.isUpdate = true;

    // touch all control to show error
    this.form.markAllAsTouched();

    // udpate carid
    this.input.carid = this.input.Key;

    // check form pass all validate 
    if (!this.form.invalid) {
      // if type = 0 insert else update
      if (this.type == 0) {
        this.subscription.push(
          this.sinhVienService.add(this.input).subscribe(
            (data) => {
              this.commonService.showSuccess('Xử lý thêm thành công !');
              this.dialogRef.close(true); 
            })
        );


      }
    } else {
      // if type = 0 insert else update
      if (this.type == 1) {
        // update
        this.subscription.push(
          this.sinhVienService.update(this.input.Key, this.input).subscribe(data => {
            if (data) {
              this.commonService.showSuccess('Xử lý cập nhật thành công !');
              this.dialogRef.close(true); 
            }
          })
        );
      }

    }
  }
}
