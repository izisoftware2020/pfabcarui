import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: '',
        loadChildren: () =>
            import('./c7-fabcar/c7-fabcar.module').then((m) => m.C7FabcarModule),
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {
        initialNavigation: 'enabled',
        // useHash: true
    })],
    exports: [RouterModule]
})
export class AppRoutingModule { }
