import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { JwtInterceptor, ServerErrorInterceptor } from './interceptors';
import { AuthService } from './services/base/00auth.service';
import { CommonService } from './utils/common.service';
import { FabcarService } from './services/base/70district.service';
import { DatePipe } from '@angular/common';

@NgModule({
  declarations: [],
  imports: [CommonModule, HttpClientModule],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ServerErrorInterceptor,
      multi: true,
    },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    CommonService,
    AuthService,
    DatePipe,
    FabcarService,

  ],
})
export class CoreModule { }
