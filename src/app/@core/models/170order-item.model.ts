export class OrderItem {
    id?: Number;
    idOrder?: string;
    idProduct?: string;
    quantity?: string;
    price?: string;
    createdAt?: string;
    updatedAt?: string     
}
