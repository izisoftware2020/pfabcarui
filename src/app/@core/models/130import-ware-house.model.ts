export class ImportWareHouse {
    id?: Number;
    idWarehouse?: string;
    idProduct?: string;
    dateImport?: string;
    quantity?: string;
    priceImport?: string     
}
