export class Transaction {
    id?: Number;
    idUser?: string;
    title?: string;
    money?: string;
    action?: string;
    status?: string;     
}