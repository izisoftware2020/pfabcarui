export class Context {
    id?: Number;
    name?: string;
    thumbnail?: string;
    content?: string;
    beginContext?: string;
    endContext?: string     
}
