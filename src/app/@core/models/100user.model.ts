export class User {
    id?: Number;
    idUser?: string;
    idRole?: string;
    idOptionalRole?: string;
    fullname?: string;
    username?: string;
    password?: string;
    sex?: string;
    avatar?: string;
    born?: string;
    phone?: string;
    email?: string;
    address?: string;
    citizenIdentification?: string;
    incomeCurrent?: string;
    percentCondition?: string;
    status?: string;
    createdAt?: string;
    updatedAt?: string;
    endateAt?: string     
}
