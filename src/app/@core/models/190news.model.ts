export class News {
    id?: Number;
    name?: string;
    image?: string;
    body?: string;
    tags?: string;
    createAt?: string;
    updateAt?: string;
    endAt?: string;
    idCategoryNews?: string     
}
