export class User {
    id?: Number;
    account?: string;
    role?: string;
    fullname?: string;
    username?: string;
    password?: string;
    sex?: string;
    avatar?: string;
    born?: string;
    phone?: string;
    email?: string;
    isEmailVerified?: string;
    address?: string;
    status?: string;
    cmnd?: string;
    famalyId?: string;
    incomeCurrent?: string;
    percentCondition?: string;
    referralCode?: string;
    createdAt?: string;
    updatedAt?: string;
    endDateAt?: string;
}
