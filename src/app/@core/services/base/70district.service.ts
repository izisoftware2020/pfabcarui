import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { District } from '../../models/70district.model';
import { CONSTAINT_UTILS } from '../../utils/constaint.utils';

@Injectable({
  providedIn: 'root'
})

export class FabcarService {

  // Define API
  apiURL = 'https://fabcar.crudcode.tk/api';

  constructor(private http: HttpClient) { }

  /*========================================
    Begin CRUD Methods for consuming RESTful API
  =========================================*/

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  /**
   * HttpClient API get() method => Fetch districts list
   * @returns 
   */
  get(): Observable<any> {
    return this.http.get<any>(this.apiURL + '/queryallcars')
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  /**
   * HttpClient API post() method => Create district
   * @param district 
   * @returns 
   */
  add(district: any): Observable<any> {
    return this.http.post<any>(this.apiURL + '/addcar', JSON.stringify(district), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  /**
   * HttpClient API put() method => Update district
   * @param id 
   * @param district 
   * @returns 
   */
  update(id: any, district: any): Observable<any> {
    return this.http.put<any>(this.apiURL + '/changeowner/' + id, JSON.stringify(district), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  /**
   * HttpClient API delete() method => Delete district
   * @param id 
   * @returns 
   */
  delete(id: any) {
    return this.http.delete<any>(this.apiURL + '/districts/' + id, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  /**
  * HttpClient API get() method => Fetch district
  * @param id 
  * @returns 
  */
  find(id: any): Observable<any> {
    return this.http.get<any>(this.apiURL + '/query/' + id)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  /**
  * HttpClient API get() method => paginate district with start page = 1 and limit
  * @param id 
  * @returns 
  */
  paginate(page: Number, limit: Number, filter: any): Observable<any[]> {
    let url = this.apiURL + '/districts/paginate?page=' + page + '&limit=' + limit;

    // add condition filter
    if (filter != '') {
      url = this.apiURL + '/districts/paginate?page=' + page + '&limit=' + limit + filter;
    }
    return this.http.get<any[]>(url)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  /*========================================
    Begin Custom Methods for RESTful API
  =========================================*/




  /**
   * Error handling 
   * @param error 
   * @returns 
   */
  handleError(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }

}
